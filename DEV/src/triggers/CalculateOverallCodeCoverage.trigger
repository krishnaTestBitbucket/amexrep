trigger CalculateOverallCodeCoverage on FeedItem (after insert)
{
    if(Trigger.isAfter && Trigger.isInsert)
    {
        CalculateOverallCodeCoverage.calculateCodeCoverageAndSendMail();
    }
}