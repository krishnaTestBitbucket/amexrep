/*
Copyright (c) 2016 ICBC
All rights reserved.



/*
*    Author : krishna_kumar18@infosys.com
*    CreatedDate : 29-May-2016
*    Class Name : AutoCompleteV2_Con
**/
public class AutoCompleteV2_Con {

    public String labelFieldVar{ get; set; }
    public String valueFieldVar{ get; set; }
    public String sObjVal{get;set;}
    public Integer randomJsIden{get;set;}
    public Object cacheField{get;private set;} 
    
    private Object targetFieldVar;
    

    public AutoCompleteV2_Con(){
        randomJsIden = getRandomNumber(1000000);
        sObjVal='Account';
        labelFieldVar='Name';
        valueFieldVar='Id';
    }
    
    /*Dummy setter Method*/
    public void setCacheField(Object cacheField){}
    
    public void setTargetFieldVar(Object targetFieldVar){
        
        if(targetFieldVar != this.targetFieldVar){
            cacheField = getCacheFieldValue(targetFieldVar);
            this.targetFieldVar = targetFieldVar;
           
        }
        
    }
    
    public Object getTargetFieldVar(){
        return targetFieldVar;
    }
    
    private Object getCacheFieldValue(Object targetFieldVar){
        Object retVal = targetFieldVar;
        if(targetFieldVar!=null){
             System.debug('Inside getCacheFieldValue->valueFieldVar ::'+valueFieldVar);
             System.debug('Inside getCacheFieldValue->labelFieldVar ::'+labelFieldVar);
             System.debug('Inside getCacheFieldValue->sObjVal ::'+sObjVal);
            
           // for(sObject sObj : Database.query('SELECT '+valueFieldVar+','+labelFieldVar+' FROM '+sObjVal+' WHERE '+valueFieldVar+' =:targetFieldVar')){
               
            
            for(sObject sObj : Database.query('SELECT '+valueFieldVar+','+labelFieldVar+','+'Company_Name__c,Author__c,Sector__c FROM '+sObjVal+' WHERE '+valueFieldVar+' =:targetFieldVar')){
              retVal = sObj.get(labelFieldVar);
              
                
                System.debug('Inside getCacheFieldValue->retVal ::'+retVal);
                
                
                break;
            }
        }
        
        return retVal;
    }

    
    /*
    *Random number generator to change the js function name if multiple components us
    ***/
    private Integer getRandomNumber(Integer size){
        Double d = Math.random() * size;
        return d.intValue();
    }
    
    /*
    *This method queries data according to the passed parameters
    ***/
    @RemoteAction
    public static List<AutoCompleteData> getData(String sObjVal,String labelFieldVar,String valueFieldVar,String param){
       
        System.debug('Inside getData->sObjVal ::'+sObjVal);
        System.debug('Inside getData->labelFieldVar ::'+labelFieldVar);
        System.debug('Inside getData->valueFieldVar ::'+valueFieldVar);
        System.debug('Inside getData->param ::'+param);
        
        
        List<AutoCompleteData> AutoCompleteDatas = new List<AutoCompleteData>();
        param = String.escapeSingleQuotes(param);
        for( Sobject sObj : Database.query('SELECT '+valueFieldVar+','+labelFieldVar+','+'Company_Name__c,Author__c,Sector__c FROM '+sObjVal+' WHERE '+labelFieldVar+' LIKE \'%'+param+'%\'')){
         // AutoCompleteDatas.add(new AutoCompleteData(sObj.get(labelFieldVar)+'--'+sObj.get('Author__c')+'--'+sObj.get('Sector__c'),sObj.get(labelFieldVar)+'--'+sObj.get('Author__c')+'--'+sObj.get('Sector__c')));
            AutoCompleteDatas.add(new AutoCompleteData(sObj.get(labelFieldVar),sObj.get(labelFieldVar)));
       }
        System.debug('Inside getData->AutoCompleteDatas ::'+AutoCompleteDatas);
        return AutoCompleteDatas;
        
    }
    
    public class AutoCompleteData{
        public String id;
        public String text;
        
        public AutoCompleteData(Object id, Object text){
            this.id = String.valueOf(id);
            this.text = String.valueOf(text);
        }
    }

    
}