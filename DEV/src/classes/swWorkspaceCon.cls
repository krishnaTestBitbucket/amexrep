public class swWorkspaceCon {
	List<ContentWorkspaceDoc> ws;
	public swWorkspaceCon() {
		ws = getRecords();
	}
	public List<ContentWorkspaceDoc> getWs() {
		if(ws == null) ws = getRecords();
		return ws;
	}
	private List<ContentWorkspaceDoc> getRecords() {
		return [SELECT ContentWorkspaceID, ContentWorkspace.Name, ContentDocumentID, ContentDocument.Title, 
                ContentDocument.LatestPublishedVersion.ID, ContentDocument.LatestPublishedVersion.Title, 
                ContentDocument.LatestPublishedVersion.VersionNumber FROM ContentWorkspaceDoc 
                WHERE ContentDocumentId IN (SELECT ContentDocumentId FROM ContentVersion )
                ORDER BY ContentWorkspace.Name, ContentDocument.Title];
	}
}