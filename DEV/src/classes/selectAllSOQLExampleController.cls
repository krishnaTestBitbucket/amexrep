public class selectAllSOQLExampleController {
    public String SobjectApiName = 'Position__c';
    public List<Position__c> accList{get;set;}
    public String query{get;set;}
    public selectAllSOQLExampleController(){
    }
    public void fetch(){
        //String SobjectApiName = 'Position__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
 
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        System.debug('============commaSepratedFields =========================');  
        System.debug('commaSepratedFields --------------------- '+commaSepratedFields );
        
        query = 'select ' + commaSepratedFields + ' from ' + SobjectApiName + ' Limit 5';
 
        accList = Database.query(query);
 
        System.debug('=====================================');  
        System.debug('accList --------------------- '+accList );
    }
}