public with sharing class SaveAndOpenPDF {
    public String recordId {
        get {
            return ApexPages.currentPage().getParameters().get('Id');
        }
    }
    public String accountId{get;set;}
    public String pdfName {get;set;}
    public ContentVersion cv { get; set; }
    public String email {get;set;}
    
    public void saveAndOpenPDF() {
        //public PageReference saveAndOpenPDF() {
        if (String.isBlank(ApexPages.currentPage().getParameters().get('open'))) {
            openPDF();
        } else if (String.isBlank(ApexPages.currentPage().getParameters().get('download'))) {
             downloadpdf();
        } else if (String.isBlank(ApexPages.currentPage().getParameters().get('email'))) {
           sendPdf();
            
        } else if (String.isBlank(ApexPages.currentPage().getParameters().get('copytolib'))) {
            // openPDF();
            
        } else  if (String.isBlank(ApexPages.currentPage().getParameters().get('attach'))) {
             //openPDF();
        }
        else{
           
        }
    }
  
    public PageReference openPDF() {
        Attachment attachment = new Attachment();
        attachment.ParentId = recordId;
        attachment.name = 'PDF_'+String.valueof(Datetime.now())+'.pdf';
        PageReference pdf = Page.TestVF;
        pdf.getParameters().put('Id', recordId);
        pdf.getParameters().put('displayOnly', '1');
        pdf.setRedirect(true);
        try {
            attachment.Body = pdf.getContent();
        }
        catch (VisualForceException e) {
            attachment.Body = Blob.valueof('There was an error.');
        }
        attachment.ContentType = 'application/pdf';
        insert attachment;
       // return attachment.Id;
        PageReference ret = new PageReference('/servlet/servlet.FileDownload?file=' + attachment.Id);
        ret.setRedirect(true);
        return ret;
    }
    
    
    public void downloadpdf() {
        //Account acc = (Account)controller.getRecord();
        //String accNum = acc.AccountNumber;
        //Assign "Account_[Ac].pdf" as a file name
        String fileName = 'Account_.pdf';
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=' + fileName);
    }
    
    
//move pdf to Library
    public pagereference movepdf()
    {
        //PageReference pdf = Page.PdfGeneratorTemplate;
        PageReference pdf = Page.Quote;
        // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('id',accountId);
        
        // create the new attachment
        Attachment attach = new Attachment();
        
        // the contents of the attachment from the pdf
        Blob body;
        
        try {
            
            // returns the output of the page as a PDF
            body = pdf.getContent();
            
            // need to pass unit test -- current bug    
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
        
        attach.Body = body;
        // add the user entered name
        attach.Name = pdfName;
        attach.IsPrivate = false;
        // attach the pdf to the account
        attach.ParentId = accountId;
        //insert attach;
        
        //For libraries....
        ContentVersion cv = new ContentVersion();
        cv.versionData = body;
        cv.title = 'Test Title';
        cv.pathOnClient = pdfName;
        try
        {
            insert cv;
        }
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Document in Library'+e.getMessage()));
            return null;
        }
        finally
        {
            cv= new ContentVersion();
        }
        
        // send the user to the account to view results
        return new PageReference('/'+accountId);
        
    }
 
  //email pdf  
  public PageReference sendPdf() {

    PageReference pdf = Page.PdfGeneratorTemplate;
    // add parent id to the parameters for standardcontroller
    pdf.getParameters().put('id',accountId);

    // the contents of the attachment from the pdf
    Blob body;

    try {

      // returns the output of the page as a PDF
      body = pdf.getContent();

    // need to pass unit test -- current bug  
    } catch (VisualforceException e) {
      body = Blob.valueOf('Some Text');
    }

    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
    attach.setContentType('application/pdf');
    attach.setFileName('testPdf.pdf');
    attach.setInline(false);
    attach.Body = body;

    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setUseSignature(false);
    mail.setToAddresses(new String[] { email });
    mail.setSubject('PDF Email Demo');
    mail.setHtmlBody('Here is the email you requested! Check the attachment!');
    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach }); 

    // Send the email
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Email with PDF sent to '+email));

    return null;

  }
}