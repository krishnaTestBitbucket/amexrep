@isTest(seealldata=true)
public class CalculateOverallCodeCoverageTest{
    
    public static testMethod void testFeedItemCodeCoverage()
    {
        //CollaborationGroup oCollaborationGroup = new CollaborationGroup(Name = Label.MVC_FoundationalDevOpsCIGroupName + 'Test');
        //oCollaborationGroup.CollaborationType = 'Public';
        //insert oCollaborationGroup;
        
        CollaborationGroup oCollaborationGroup = [SELECT Id FROM CollaborationGroup WHERE Name = 'DeploymentStatus'];
        
        FeedItem oFeedItem = new FeedItem();
        oFeedItem.Body = 'Build: SUCCESS';
        oFeedItem.ParentID = oCollaborationGroup.Id;
        
        Test.startTest();
        insert oFeedItem;
        Test.stopTest();       
    }
}