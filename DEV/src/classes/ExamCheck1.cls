public class ExamCheck1{

    List<Exam__c> result;
    public String dId{get;set;}
    public String eId{get;set;}  
    public Exam__c newExam{get;set;}

    /*public String getLid() {
        return null;
    }*/

  
    public List<Exam__c> getResult(){
        return result;
    }

   
   
    public ExamCheck1(){
        System.debug('================Inside ExamCheck1====================='); 
        newExam = new Exam__c();
        result = [SELECT Id,Name , Active__c FROM Exam__c order by CreatedDate asc];
        System.debug('================Inside ExamCheck1====================='+result);
        
    }           
   
   
    public void deleteRow(){
        System.debug('================Inside deleteRow=====================');  
        System.debug('dId is--------------------- '+dId);
        Exam__c objExam = [SELECT Id,Name , Active__c FROM Exam__c WHERE id =:dId];
        System.debug('objExam is--------------------- '+objExam);
        delete objExam ;
        result = [SELECT Id,Name , Active__c FROM Exam__c order by CreatedDate asc];

    }  
       
    
    public void edit(){
        System.debug('================Inside edit=====================');
        System.debug('Inside edit eId is--------------------- '+eId);
        newExam = [SELECT Id,Name , Active__c FROM Exam__c WHERE id =:eId];
        //update newExam;
        
    }  
       

    
    public void save(){
        System.debug('================Inside save=====================');
        upsert newExam;
        result = new List<Exam__c>();
        result = [SELECT Id,Name , Active__c FROM Exam__c order by CreatedDate asc];
        newExam = new Exam__c();
        //return null;
    }

    
    
    public PageReference cancel(){
        System.debug('================Inside cancel=====================');
        newExam = new Exam__c();
        return null;
    }

}