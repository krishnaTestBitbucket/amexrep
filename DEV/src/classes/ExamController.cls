public with sharing  class ExamController {

    public String getAllExamList() {
        return null;
    }


    public PageReference edit() {
        return null;
    }


   
    private ApexPages.StandardController sc;
    
    
    //Variable to hold all exam added/edited
    public List<Exam__c> allExamList = new List<Exam__c>();
    
    //Variable to hold add exam to be deleted
    public List<Exam__c> deleteExamList = new List<Exam__c>();
    
    //Variable to hold the Exam record
    public Exam__c examRec {get;set;}
    
    // the variable being set from the commandlink
    public String index{get; set;}
    
   public ExamController(ApexPages.StandardController sc) {
         this.sc = sc;
        allExamList = [SELECT Id, Name, Active__c FROM Exam__c]; 
    } 
    
    
    public ExamController () {
       allExamList = [SELECT Id, Name, Active__c FROM Exam__c];  
    } 
    
    public void save() {
    insert examRec ;
       allExamList = [SELECT  Name, Active__c FROM Exam__c];  
       
      
    }
    
    
    public PageReference cancel()
    {
    PageReference page = new PageReference('/apex/Exam');
    page.setRedirect(true);
    return page;
    }



    public PageReference deleterecord()
    {
     System.debug('nickName: '+index);
     //Remove a exam from the table.
         Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('index'));
         System.debug('Index value is : '+index);
    //If the exam is an existing contact then add it to the list to delete from the databse
        if(allExamList [indexVal - 1].Id != null)
        deleteExamList.add(allExamList [indexVal - 1]);
        //Remove the contact from the table    
            allExamList .remove(indexVal - 1);            

    
     //update existing contacts and insert new ones
        upsert allExamList ;
    //delete the contacts that were removed
        if(deleteExamList.size() > 0)
        delete deleteExamList;
    
     
     PageReference page = new PageReference('/apex/Exam');
     page.setRedirect(true);
     return page;
    }



    public List<Exam__c> getExams() {
    if(allExamList == null) {
    allExamList = [SELECT Id, Name, Active__c FROM Exam__c];
    }
    return allExamList ;
    }
}