public class QuestionCheck
{
    public Question__c newQues{get;set;}
    public Answer__c oAns{get;set;}
    public List<Question__c> addRowList{get;set;}
    public List<Answer__c> ansList{get;set;}
    // public List<Question__c> qList{get;set;}
    public String dId{get;set;}
    public String eId{get;set;}  
    public List<Question__c> qEditList{get;set;}
    public Integer rowIndex {get;set;}
    
    public List<cExam> examList {get; set;}
    
    //---------pagination code starts-------------	
    Public Integer size{get;set;} 
    Public Integer noOfRecords{get; set;} 
    public List<SelectOption> paginationSizeOptions{get;set;}
    //---------pagination code ends-------------	
    
    
    public QuestionCheck(){
        System.debug('================Inside QuestionCheck=====================');    
        newQues= new Question__c();
        oAns = new Answer__c();
        
        addRowList = new List<Question__c>();
        examList=new List<cExam>();
        ansList = new List<Answer__c>();
        ansList.add(new Answer__c());
        // qList = [select id, name,MCQ__c,(select name,Valid__c from Answers__r order by name asc) from Question__c ];
        
        for(Exam__c c: [select Id, Name,Active__c from Exam__c  order by name asc]) {
            examList.add(new cExam(c));
        }
        //---------pagination code starts-------------	
        size=5;
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('3','3'));
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        //---------pagination code ends-------------		
        
        
        
    }
    public void AddRow()
    {
        System.debug('================Inside AddRow=====================');
        ansList.add(new Answer__c());
        
    }
    
    public void deleteRow() 
    {  
        
        //dId = Apexpages.currentpage().getParameters().get('paramname');
        System.debug('================Inside deleteRow=====================');
        Question__c objQue = [SELECT name, MCQ__c, id FROM Question__c WHERE id =:dId];
        delete objQue;
        // qList = [select id, name,MCQ__c,(select name,Valid__c from Answers__r) from Question__c ];
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
            [select id, name,MCQ__c,(select name,Valid__c from Answers__r order by name asc) from Question__c]));
    }
    
    
    //Remove a contact from the table.
    public void removeAnswer(){
        System.debug('================Inside removeAnswer=====================');
        // rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        System.debug('================Row deleted index====================='+(rowIndex-1));
        System.debug('================Before deletion====================='+ansList);
        System.debug('================Row to be deleted====================='+ansList[rowIndex-1]);
        ansList.remove(rowIndex);
        //ansList.add(del);
        System.debug('================After deletion====================='+ansList);
    }  
    
    
    
    
    public void edit() 
    {
        System.debug('================Inside edit=====================');
        newQues = [select id, name,MCQ__c,
                   (select name, valid__c from Answers__r) from Question__c WHERE id =:eId];
        ansList=(newQues.answers__r);
        examList=new List<cExam>();
        
        for(Exam__c c: [select Id, Name,Active__c from Exam__c ]) {
            cExam editExam=new cExam(c);
            
            for(Exam_Question__c examQues: 
                [select id, exam__c, Question__c from Exam_Question__c where (Question__c =:eId AND exam__c=:c.id)]) {
                    editExam.selected=true;
                    
                }
            examList.add(editExam);
        }
    }  
    
    public class cExam {
        
        public Exam__c exam {get; set;}
        public Boolean selected {get; set;}
        
        public cExam(Exam__c c) {
            exam = c;
            selected = false;
        }
    }
    
    
    
    public void save()
    {
        System.debug('================Inside save=====================');
        // addRowList.add(newQues);
        // insert addRowList;
        System.debug('================newQues before save=====================i= '+newQues);
        
        upsert newQues;
        processSelected(newQues.id);
        System.debug('================newQues after save=====================i= '+newQues); 
        
        List<Answer__C> lstInsertNewAnswer = new List<Answer__C>();
        
        // for(integer i = 0; i < addRowList.size(); i++ )
        // {
        // System.debug('================First Loop=====================i= '+i); 
        
        for(integer j = 0; j < ansList.size(); j++ )
        {
            System.debug('================Second Loop====================== '+j 
                         + ' id= '+ newQues.id
                         + ' Name= '+ newQues.Name
                         + ' MCQ__c= '+newQues.MCQ__c
                        ); 
            
            
            if(ansList[j].Name != null && ansList[j].Name.length() != 0){
                Answer__c oAns1 =  new Answer__c();
                //oAns1.Question__c = addRowList[i].id;
                oAns1.Question__c = newQues.id;
                oAns1.Name = ansList[j].Name;
                oAns1.Valid__c = Boolean.ValueOf(ansList[j].Valid__c);
                lstInsertNewAnswer.add(oAns1);
            }
            
            
            
        }
        
        // }
        
        //Going to Delete all answer records abefore editing it
        System.debug('================newQues.id=====================i= '+newQues.id); 
        
        Answer__c[] obgDelete = [select id,Question__c,name,Valid__c from Answer__c WHERE Question__c =:newQues.id];
        
        if (obgDelete.size() > 0)
        {
            System.debug('================Inside save going to delete answer=====================');
            delete obgDelete;
            
            System.debug('================going to edit all answer list at a time lstInsertNewAnswer====================='+lstInsertNewAnswer);
            insert lstInsertNewAnswer;
            
        }else{
            System.debug('================going to save all answer list at a time lstInsertNewAnswer====================='+lstInsertNewAnswer);
            insert lstInsertNewAnswer;
        }
        
        
        // qList = [select id, name,MCQ__c,(select name,Valid__c from Answers__r order by name asc) from Question__c ];
        //System.debug('================going to fetch updated qList====================='+qList);
        newQues= new Question__c();
        ansList = new List<Answer__c>();
        ansList.add(new Answer__c());
        examList = new List<cExam>();
        for(Exam__c c: [select Id, Name,Active__c from Exam__c order by name asc]) {
            examList.add(new cExam(c));
        } 
        System.debug('================going back to page examList ====================='+examList);
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
            [select id, name,MCQ__c,(select name,Valid__c from Answers__r order by name asc) from Question__c]));
        setCon.setPageSize(size);
        //refreshPageSize();
    }
    
    
    public PageReference Cancel()
    {
        System.debug('================Inside Cancel=====================');
        PageReference pageRef = new PageReference('/apex/Question');
        return pageRef.setRedirect(true);
    }
    
    
    
    public void processSelected(ID id) {
        System.debug('================Inside processSelected====================='+id);
        Exam_Question__c[] obgDeleteExamQuestion = [select  id, exam__c, Question__c from Exam_Question__c where Question__c=:id];
        
        if (obgDeleteExamQuestion.size() > 0)
        {
            System.debug('================Inside processSelected going to delete Exam_Question__c=====================');
            delete obgDeleteExamQuestion;
            
            System.debug('================going to call in if insertExamQuestion=====================');
            insertExamQuestion(id);
            
        }else{
            System.debug('================going to call in else insertExamQuestion=====================');
            insertExamQuestion(id);
        }
    }    
    
    
    public void insertExamQuestion(ID id) {
        
        
        List<Exam__c> selectedExams = new List<Exam__c>();
        System.debug('================Inside insertExamQuestion getExams()====================='+examList);
        
        for(cExam cCon: examList) {
            if(cCon.selected == true) {
                selectedExams.add(cCon.exam);
            }
        }
        
        List<Exam_Question__c> insertExamQuestion = new List<Exam_Question__c>();
        for(Exam__c con: selectedExams) {
            Exam_Question__c junctionObj =  new Exam_Question__c();
            System.debug('================selectedExams are::  ====================== '+con 
                         + ' id= '+ con.id
                         + ' Name= '+ id
                        );
            
            
            
            junctionObj.Exam__c = con.id;
            junctionObj.Question__c  = id;
            insertExamQuestion.add(junctionObj);
            
        }
        insert insertExamQuestion;
    }
    
    //---------pagination code starts-------------	
    public ApexPages.StandardSetController setCon {
        get {
            System.debug('================Inside setCon=====================');
            if(setCon == null) {                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [select id, name,MCQ__c,(select name,Valid__c from Answers__r order by name asc) from Question__c]
                ));
                setCon.setPageSize(size);  
                noOfRecords = setCon.getResultSize();
            }            
            return setCon;
        }
        set;
    }
    
    
    //Changes the size of pagination
    public PageReference refreshPageSize() {
        setCon.setPageSize(size);
        //qList= setCon.getRecords();
        return null;
    }
    
    // Initialize setCon and return a list of record    
    
    public List<Question__c> getQuestions() {
        return (List<Question__c>) setCon.getRecords();
    }
    
    
    //---------pagination code ends-------------  
}