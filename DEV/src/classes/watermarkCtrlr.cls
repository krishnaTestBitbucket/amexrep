public class watermarkCtrlr {
    
    public String accountId{get;set;}
    //public Attachment att{get;set;}
    public String pdfName {get;set;}
    
    public ContentVersion cv { get; set; }
    
    public watermarkCtrlr()
    {
        // not used..
    }
    
    public pagereference saveaction()
    {
        //PageReference pdf = Page.PdfGeneratorTemplate;
          PageReference pdf = Page.Quote;
        // add parent id to the parameters for standardcontroller
        pdf.getParameters().put('id',accountId);
        
        // create the new attachment
        Attachment attach = new Attachment();
        
        // the contents of the attachment from the pdf
        Blob body;
        
        try {
            
            // returns the output of the page as a PDF
            body = pdf.getContent();
            
            // need to pass unit test -- current bug    
        } catch (VisualforceException e) {
            body = Blob.valueOf('Some Text');
        }
        
        attach.Body = body;
        // add the user entered name
        attach.Name = pdfName;
        attach.IsPrivate = false;
        // attach the pdf to the account
        attach.ParentId = accountId;
        //insert attach;
        
        //For libraries....
        ContentVersion cv = new ContentVersion();
        cv.versionData = body;
        cv.title = 'Test Title';
        cv.pathOnClient = pdfName;
        try
        {
            insert cv;
        }
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Document in Library'+e.getMessage()));
            return null;
        }
        finally
        {
            cv= new ContentVersion();
        }
        
        // send the user to the account to view results
        return new PageReference('/'+accountId);
        
    }
}