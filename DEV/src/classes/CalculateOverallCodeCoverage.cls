public class CalculateOverallCodeCoverage 
{
    public static Set<Id> setFeeds = new Set<Id>();
    public static List<FeedItem> lstParentNames = new List<FeedItem>();
    public static List<String> lstEmail = new List<String>();
    public static Set<Id> idSet = new Set<Id>();
    public static List<User> lstUserEmails = new List<User>();
    public static List<CollaborationGroupMember> lstMember = new List<CollaborationGroupMember>();
    public static String plainTextBody {get;set;}
    public static String subjectTextBody {get;set;}
    public static boolean flag = false;
    public static Integer overallCoverage {get;set;}
    public static String excMessage {get;set;}
    public static String addToFeed {get;set;}
    public static List<FeedItem> lstFeed {get;set;}
    
    public static void bodyForEmail()
    {
        plainTextBody = '';
        
        if(flag)
        {           
            plainTextBody += 'Dear Admin \n\n';
            plainTextBody += 'The Code Coverage of '+ Label.SandBoxName + ' is :: ' +overallCoverage + ' %' + '\n\n\n';
            plainTextBody += 'Thanks, \n';
            plainTextBody += 'Infosys DevOps Team';
            subjectTextBody = Label.SandBoxName +' :: ' + ' ' +overallCoverage + ' %';
        }
        else
        {
            plainTextBody += 'Dear Admin, \n\n';
            plainTextBody +=  Label.FoundationalDevOpsCIException;
            plainTextBody +=  excMessage + '\n\n\n';
            plainTextBody += 'Thanks, \n';
            plainTextBody += 'Infosys DevOps Team';
        }       
    }
    
    public static void calculateCodeCoverageAndSendMail()
    {
        lstFeed = new List<FeedItem>();
        for(FeedItem oFeedItem : (List<FeedItem>)Trigger.New)
        {
           setFeeds.add(oFeedItem.Id);
        }
       
        lstParentNames = [SELECT parent.name, body FROM FeedItem WHERE Id IN :setFeeds];
        
        for(FeedItem oFeed : lstParentNames)
        {
           if(oFeed.parent.name.contains(Label.DeploymentGroupName) && oFeed.body.contains('Build:') && oFeed.body.contains('SUCCESS'))
           {
               getCodeCoverage(UserInfo.getSessionID(), oFeed.Id);
           }
        }
    }
    
    public static void getMemberEmails()
    {
        Id oId = [SELECT Id FROM CollaborationGroup WHERE Name = :Label.DeploymentGroupName].Id;       
        lstMember = [SELECT memberId FROM CollaborationGroupMember WHERE CollaborationGroupId = :oId];       
        
        for(CollaborationGroupMember oColl : lstMember)
        {
            idSet.add(oColl.memberId);
        }
        
        lstUserEmails = [SELECT Email FROM User WHERE Id IN :idSet];
        
        for(User oUser : lstUserEmails)
        {
            lstEmail.add(oUser.Email);
        }
    }
    
    @future(callout=true)
    public static void getCodeCoverage(String sId, Id feedId)
    {
        try
        {
            Integer oPercCov;
            String queryStr = 'SELECT+PercentCovered+FROM+ApexOrgWideCoverage';
            String ENDPOINT = 'https://' + System.URL.getSalesforceBaseUrl().getHost() + '/services/data/v35.0/tooling/';
            HttpRequest req = new HttpRequest();
            req.setEndpoint(ENDPOINT + 'query/?q=' + queryStr);
            req.setHeader('Authorization', 'Bearer ' + sId);
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            req.setMethod('GET');
            req.setTimeout(80000);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            
            ToolingResponse oToolingResponse = (ToolingResponse)JSON.deserialize(res.getBody(), ToolingResponse.class);
            
            for(Record oRec : oToolingResponse.records)
            {
                oPercCov = oRec.PercentCovered;
            }
            
            addToFeed = Label.DeploymentGroupName + ' ' +oPercCov + '%';
            lstFeed = [SELECT body FROM FeedItem WHERE Id = :feedId];
            lstFeed[0].body = lstFeed[0].body + '\n\n' +addToFeed;
            update lstFeed;
            
            sendMailToAdminOnSuccess(oPercCov);
        }
        catch(Exception ex)
        {
            sendMailToAdminOnFailure(ex.getMessage());
        }
    }
    
    public static void sendMailToAdminOnSuccess(Integer resStr)
    {
        getMemberEmails();
        flag = true;
        overallCoverage = resStr;
        bodyForEmail();       
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subjectTextBody);
        email.setPlainTextBody(plainTextBody);
        email.setToAddresses(lstEmail);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }
    
    public static void sendMailToAdminOnFailure(String excMsg)
    {
        getMemberEmails();
        excMessage = excMsg;
        bodyForEmail();       
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(Label.FoundationalDevOpsCIException);
        email.setPlainTextBody(plainTextBody);
        email.setToAddresses(lstEmail);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }
    
    public class Attributes
    {
        public string type { get; set; }
        public string url { get; set; }
    }
    
    public class Record
    {
        public Attributes attributes { get; set; }
        public integer PercentCovered { get; set; }
    }
    
    public class ToolingResponse
    {
        public integer size { get; set; }
        public integer totalSize { get; set; }
        public boolean done { get; set; }
        public sobject queryLocator { get; set; }
        public string entityTypeName { get; set; }
        public List<Record> records { get; set; }
    }
}