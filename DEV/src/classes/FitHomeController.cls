public class FitHomeController {
    public TokenResponse tokResp{get;set;}
    public DataSourceList dsl{get;set;}
    public String dataStreamId{get;set;}
    public DataSet dsVal{get;set;}
    
    public FitHomeController(){
        
    }
    
    public PageReference authenticate(){
        if(ApexPages.currentPage().getParameters().get('code') == null && ApexPages.currentPage().getParameters().get('error') == null){
            String ep = 'https://accounts.google.com/o/oauth2/v2/auth?';
            ep += 'scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Ffitness.activity.read';
            ep += '&response_type=code';
            ep += '&client_id=280747877024-08ehk5bv6fcb2tu4j5f2ai0nghs9dcod.apps.googleusercontent.com';
            ep += '&redirect_uri=https%3A%2F%2Fmyinfi-dev-ed.my.salesforce.com%2Fapex%2FFitHome';
            return new PageReference(ep);
        }
        else if(ApexPages.currentPage().getParameters().get('error') != null){
            ApexPages.addMessage(new ApexPages.message(ApexPages.SEVERITY.ERROR,ApexPages.currentPage().getParameters().get('error')));
            return null;
        }
        else{
            HttpRequest request = new HttpRequest();
            request.setMethod('POST');
            request.setEndpoint('https://www.googleapis.com/oauth2/v4/token');
            String reqBody = 'code='+ApexPages.currentPage().getParameters().get('code');
            reqBody += '&client_id=280747877024-08ehk5bv6fcb2tu4j5f2ai0nghs9dcod.apps.googleusercontent.com';
            reqBody += '&client_secret=bXhF2y_jwOaATVxe39luQ1V9';
            reqBody += '&redirect_uri=https%3A%2F%2Fmyinfi-dev-ed.my.salesforce.com%2Fapex%2FFitHome';
            reqBody += '&grant_type=authorization_code';
            request.setBody(reqBody);
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            HttpResponse resp = new Http().send(request);
            //system.debug(resp.getBody());
            JSONParser parser = JSON.createParser(resp.getBody());
            tokResp = (TokenResponse)parser.readValueAs(TokenResponse.class);
            getDataSourceList();
            return null; 
        }
    }
    
    public void getDataSourceList(){
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://www.googleapis.com/fitness/v1/users/me/dataSources');
        req.setHeader('Authorization', 'Bearer '+tokResp.access_token);
        req.setMethod('GET');
        HttpResponse resp = new Http().send(req);
        //system.debug(resp.getBody());
        JSONParser parser = JSON.createParser(resp.getBody());
        dsl = (DataSourceList)parser.readValueAs(DataSourceList.class);
        system.debug(dsl);
    }
    
    public void getData(){
        system.debug('dataStreamId--'+dataStreamId);
        system.debug(tokResp.access_token);
        system.debug(System.now().getTime());
        system.debug(System.now().addDays(-30).getTime());
        HttpRequest req = new HttpRequest();
        String ep = 'https://www.googleapis.com/fitness/v1/users/me/dataSources/';
        //ep += 'derived%3Acom.google.activity.sample%3Acom.google.android.gms%3ALGE%3ANexus%205%3A3dec93a2%3Adetailed';
        ep += EncodingUtil.urlEncode(dataStreamId, 'UTF-8');
        ep += '/datasets/' + (System.now().addDays(-16).getTime() * 1000000) +'-'+ (System.now().addDays(-1).getTime() * 1000000);
        req.setEndpoint(ep);
        req.setHeader('Authorization', 'Bearer '+tokResp.access_token);
        req.setHeader('Content-Length', '0');
        req.setMethod('GET');
        
        HttpResponse resp = new Http().send(req);
        system.debug(resp.getBody());
        JSONParser parser = JSON.createParser(resp.getBody());
        dsVal = (DataSet)parser.readValueAs(DataSet.class);
        system.debug(dsVal);
    }
    
    /************************************** Class to parse Token Response **************************************/
    class TokenResponse{
        public String access_token{get;set;}
        public String token_type{get;set;}
        public integer expires_in{get;set;}
        
        public TokenResponse(String access_token,String token_type,integer expires_in){
            this.access_token = access_token;
            this.token_type = token_type;
            this.expires_in = expires_in;
        }
    }
    
    /************************************** Class to parse List of Data Sources **************************************/
    class Application {
        public String packageName{get;set;}
        public String version{get;set;}
        public String detailsUrl{get;set;}
        public String name{get;set;}
        
        public Application(String packageName,String version,String detailsUrl,String name){
            this.packageName = packageName;
            this.version = version;
            this.detailsUrl = detailsUrl;
            this.name = name;
        }
    }
    
    class DataSource {
        public String dataStreamId{get;set;}
        public String name{get;set;}
        public String dataStreamName{get;set;}
        public String type{get;set;}
        public DataType dataType{get;set;}
        public Device device{get;set;}
        public Application application{get;set;}
        
        public DataSource(String dataStreamId,String name,String dataStreamName,String type,DataType dataType,Device device,Application application){
            this.dataStreamId = dataStreamId;
            this.name = name;
            this.dataStreamName = dataStreamName;
            this.type = type;
            this.dataType = dataType;
            this.device = device;
            this.application = application;
        }
    }
    
    class DataSourceList {
        public List<DataSource> dataSource{get;set;}
        
        public DataSourceList(List<DataSource> dataSource){
            this.dataSource = dataSource;
        }
    }
    
    class DataType {
        public String name{get;set;}
        public List<Field> field{get;set;}
        
        public DataType(String name,List<Field> field){
            this.name = name;
            this.field = field;
        }
    }
    
    class Device {
        public String uid{get;set;}
        public String type{get;set;}
        public String version{get;set;}
        public String model{get;set;}
        public String manufacturer{get;set;}
        
        public Device(String uid,String type,String version,String model,String manufacturer){
            this.uid = uid;
            this.type = type;
            this.version = version;
            this.model = model;
            this.manufacturer = manufacturer;
        }
    }
    
    class Field {
        public String name{get;set;}
        public String format{get;set;}
        
        public Field(String name,String format){
            this.name = name;
            this.format = format;
        }
    }
    
    /************************************** Classes to parse Data Sets **************************************/
    class DataSet {
        public Long minStartTimeNs{get;set;}
        public Long maxEndTimeNs{get;set;}
        public String dataSourceId{get;set;}
        public List<Point> point{get;set;}
        public String nextPageToken{get;set;}
        
        public DataSet(Long minStartTimeNs,Long maxEndTimeNs,String dataSourceId,List<Point> point,String nextPageToken){
            this.minStartTimeNs = minStartTimeNs;
            this.maxEndTimeNs = maxEndTimeNs;
            this.dataSourceId = dataSourceId;
            this.point = point;
            this.nextPageToken = nextPageToken;
        }
    }
    
    class Point {
        public Long startTimeNanos{get;set;}
        public Long endTimeNanos{get;set;}
        public String dataTypeName{get;set;}
        public String originDataSourceId{get;set;}
        public List<Value> value{get;set;}
        public Long modifiedTimeMillis{get;set;}
        public Long rawTimestampNanos{get;set;}
        public Long computationTimeMillis{get;set;}
        public Point(Long startTimeNanos,Long endTimeNanos,String dataTypeName,String originDataSourceId,List<Value> value,Long modifiedTimeMillis,Long rawTimestampNanos,Long computationTimeMillis){
            this.startTimeNanos = startTimeNanos;
            this.endTimeNanos = endTimeNanos;
            this.dataTypeName = dataTypeName;
            this.originDataSourceId = originDataSourceId;
            this.value = value;
            this.modifiedTimeMillis = modifiedTimeMillis;
            this.rawTimestampNanos = rawTimestampNanos;
            this.computationTimeMillis = computationTimeMillis;
        }
    }
    
    class Value {
        public integer intVal{get;set;}
        public Double fpVal{get;set;}
        
        public Value(integer intVal,Double fpVal){
            this.intVal = intVal;
            this.fpVal = fpVal;
        }
        
    }
}