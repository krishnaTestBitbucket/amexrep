public with sharing class Autocomplete_V1 {
    
    // Instance fields
    public String searchTerm {get; set;}
    public String selectedMovie {get; set;}
    public List<String> companyNameList{get;set;}
    
    // JS Remoting action called when searching for a movie name
  /*  @RemoteAction
    public static List<Movie__c> searchMovie(String searchTerm) {
        System.debug('Movie Name is: '+searchTerm );
        List<Movie__c> movies = Database.query('Select Id, Name from Movie__c where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return movies;
    }
  */  
    
     // JS Remoting action called when searching for a movie name
    @RemoteAction
    public static List<Account > searchCompany(String searchTerm) {
        System.debug('Company Name is: '+searchTerm );
        List<Account > comanies= Database.query('Select Id,Company_Name__c from Account  where Company_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
        return comanies;
    }

    
     
    
    
}