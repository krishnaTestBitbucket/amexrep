public class AutocompleteV2Demo_con {

    public String targetField { get; set; }
    public List<Question__c> addRowList{get;set;}
    public List<Answer__c> ansList{get;set;}
    public Answer__c oAns{get;set;}
    public Integer rowIndex {get;set;}
    public Account B;
    
    public AutocompleteV2Demo_con(){
        System.debug('================Inside QuestionCheck=====================');    
        ansList = new List<Answer__c>();
    }
    
  /*
    public void AddRow()
    {
        System.debug('================Inside AddRow=====================');
        
        if(!String.isBlank(targetField)){
        Answer__c insertAnswer = new Answer__c();
        insertAnswer.Question__c = 'a0H28000003t03U';
        insertAnswer.Name =targetField;
        ansList.add(insertAnswer);
        //upsert ansList;
       } 
    } 
    
 */
 
   public void AddRow()
    {
        System.debug('================Inside AddRow=====================');
        
        if(!String.isBlank(targetField)){
        Answer__c insertAnswer = new Answer__c();
        insertAnswer.Question__c = 'a0H28000003t03U';
        
        B=[SELECT Name,Company_Name__c,Author__c,Sector__c FROM Account WHERE Name=:targetField];
        insertAnswer.Name =B.Company_Name__c+'->'+B.Author__c+'->'+B.Sector__c;
        ansList.add(insertAnswer);
        //upsert ansList;
       } 
    }   
       
    
    public void save(){
        System.debug('================Inside save=====================');
        if((ansList!= null && !ansList.isEmpty())){
         upsert ansList;
        }
        ansList=null;
     }
       
    public void removeAnswer(){
        ansList.remove(rowIndex);
       System.debug('================After deletion====================='+ansList);
        
    }
    
    
    
    
    
    public PageReference Cancel()
    {
        System.debug('================Inside Cancel=====================');
        PageReference pageRef = new PageReference('/apex/AutocompleteV2Demo');
        return pageRef.setRedirect(true);
    }
    
    
    
    
}